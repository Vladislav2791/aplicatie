import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

public class CurrencyType {

    String result="";
     InputStream inputStream;

    public String Currency() throws IOException{
        try {
            Properties properties=new Properties();
            String propFileName="Exchange";

            inputStream=getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream !=null){
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Property File" + propFileName + "not found in the classpath");
            }

            String mainCurrency=properties.getProperty("mainCurrency");
            String EURO=properties.getProperty("EURO");
            String DOLAR=properties.getProperty("DOLAR");

            result="Exchange List currency = " + mainCurrency + " " + "EURO = " + EURO + " " + "DOLAR = " + DOLAR;
            System.out.println(result);
        } catch (Exception e){
            System.out.println("Exception " + e);
        } finally {
            inputStream.close();
        }
        return result;
    }




}
