import java.io.*;

public class ProductType {

    private String name;
    private double price;
    private int quantity;
    private int initialStock;
    private int newStock;



    public ProductType() {

    }
    public ProductType(String name, double price, int quantity,int initialStock ) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.initialStock=initialStock;

    }

    public  static void readFruits() {
        String thisLine=null;
        try {
            File file=new File("C:\\Users\\user\\IdeaProjects\\EcommerceApp\\src\\main\\resources\\Fruits");
            BufferedReader bufferedReader=new BufferedReader(new FileReader(file));
            while ((thisLine=bufferedReader.readLine())!=null){
                System.out.println(thisLine);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }


    public String getName() {
        return name;
    }

    public double getPrice() {
        return price*quantity;
    }

    public int getNewStock() {
        return initialStock-quantity;
    }


    @Override
    public String toString() {
        return "ProductType{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}





